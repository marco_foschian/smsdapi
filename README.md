# smsdAPI #

Simple http API for sending SMS trought smsd

## configure ##

Edit options object in server.js.

Set send_dir to the folder where smsd wait for sms files.

Set accept array to filter ip allowed to send sms (comment for accept-all).

## send a sms ##

post to /send with following parameters:

to : cell number (in sms file format) where to send the sms

text: message to send (cutted to 163 characters)