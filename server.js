var minihttp = require('minihttp');
var util = require('util');
var fs = require('fs');
var fsPath = require("path");
var uuid = require('node-uuid');
var SmsFile = require('./lib/smsdFile');

var APP_DIRNAME = fsPath.dirname(require.main.filename);

var options = require('./Options');

if( !options || !options.send_dir )
{
	var msg = 'send_dir not defined in options file';
	console.log( msg );
	console.error( msg );
	process.exit(1);
}


var Promise = require( 'promise' );

//--- Promise Extender ---
//{
Promise.prototype.finally = function( callback )
{
	return this.then( function( result )
	{
		callback( null, result );
		return result;
	},
	function( err )
	{
		callback( err.toString() );
		return Promise.reject( err );
	});
};
//}


function destFilePath( basename )
{
	var fileName = fsPath.join( options.send_dir, basename ) + '.sms';
	if( fileName[0] != '/' || fileName[0] != '\\' )
		fileName = fsPath.resolve( fsPath.join(APP_DIRNAME, fileName) );
	
	return fileName;
}

function fileWrite( fileName, content )
{
	return new Promise( function(resolve, reject)
	{
		fs.writeFile( fileName, content, function (err)
		{
			if (err)
				reject( err );
			else
				resolve();
		});
	});
}

//---------------------
//	HTTP server
//{
var Server = new minihttp.HttpServer( options );

function getRequesterAddress( request )
{
	return request.headers['x-forwarded-for'] || request.connection.remoteAddress;
}

function canAcceptRequest( request )
{
	if( !options.accept )
		return true;

	var ip = getRequesterAddress( request );
	return options.accept.indexOf( ip ) >= 0 ? true : false;
}

function rejectRequest( response )
{
	var mime = {"Content-Type": 'text/html'};
	response.writeHead(404);
	response.write( '<html><body><h1>Not Found</h1></body></html>' );
	response.end();
	return;
}

function defaultCallback( resp )
{
	var response = resp;
	return function( err, data )
	{
		if(err)
			Server.sendErrorResponse( response, err );
		else
			Server.sendResponse( response, data );
	};
}
Server.route('/send',
{
	post: function (request, response, parms )
	//get: function (request, response, parms )
	{
		if( !canAcceptRequest( request ) )
		{
			rejectRequest( response );
			return;
		}

		var text = parms.fields.text;
		var to = parms.fields.to;
		
		var sms = new SmsFile( { to: to, text: text } );
		var sms_id = uuid.v4();

		var fileName = destFilePath( sms_id );
		var fileContent = sms.toString();
		//console.log( 'Writing to %s\n{\n%s\n}', fileName, fileContent );

		var callback = defaultCallback( response );
		
		fileWrite( fileName, fileContent )
		.then( function()
		{
			return { id: sms_id };
		})
		.finally( callback );
	}
});

Server.listen();

//}