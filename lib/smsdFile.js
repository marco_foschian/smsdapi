var MAXTEXTLEN = 163;

function SMS( args )
{
	if( !args ) args = {};
	
	this.text = args.text || '';
	this.to = args.to || '';

	if( this.text.length > MAXTEXTLEN )
		this.text = this.text.substring(0, MAXTEXTLEN);
}

SMS.prototype.toString = function()
{
	return 'To: '+this.to+'\n\n'+this.text+'\n';
}

module.exports = SMS;

